$(document).ready(function(){
    $(".dropdown-trigger").dropdown();
});
$(document).ready(function(){
    $('select').formSelect();
});
$(document).ready(function(){
    $('.sidenav').sidenav();
});
$(window).on('load', function(){
    $('.loader1').fadeOut(); 
    $('.loader2').fadeOut();
    $('.preload').addClass('complete');
});
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("nav-wrapper").style.top = "0";
  } else {
    document.getElementById("nav-wrapper").style.top = "-65px";
  }
  prevScrollpos = currentScrollPos;
};