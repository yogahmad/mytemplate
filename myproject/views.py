from django.shortcuts import render
from django.shortcuts import render_to_response

def not_found(request, template_name="error404.html"): 
    response = render_to_response(template_name) 
    response.status_code = 404 
    return response

def server_error(request, template_name="error500.html"): 
    response = render_to_response(template_name) 
    response.status_code = 500 
    return response

def permission_denied(request, template_name="error403.html"): 
    response = render_to_response(template_name) 
    response.status_code = 403 
    return response

def bad_request(request, template_name="error400.html"): 
    response = render_to_response(template_name) 
    response.status_code = 400 
    return response
