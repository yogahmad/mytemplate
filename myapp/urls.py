from django.urls import path
from myapp.views import * 

app_name = "home"

urlpatterns = [
    path("", index, name="home"),
]
